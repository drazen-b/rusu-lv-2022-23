'''
Zadatak 1
Napišite  python  skriptu  koja  od  korisnika  zahtijeva  unos
radnih  sati  te  koliko  je  plaćen  po  radnom  satu.
Nakon  toga izračunajte koliko je korisnik zaradio i ispišite na ekran.
'''
'''
Zadatak 3
Prepravite zadatak 1 na način da ukupni iznos izračunavate u zasebnoj
funkciji naziva total_kn.'''


def total_kn():
    hours = float(input('Radni sati: '))
    payPerHour = float(input('kn/h: '))
    print('Ukupno: ', hours * payPerHour)


'''
Zadatak 2
Napišite program koji od korisnika zahtijeva upis jednog broja
koji predstavlja nekakvu ocjenu i nalazi se između 0.0 i 1.0.
Ispišite kojoj kategoriji pripada ocjena na temelju sljedećih uvjeta:
>= 0.9 A >= 0.8 B >= 0.7 C >= 0.6 D < 0.6 F

Ako  korisnik  nije  utipkao  broj,  ispišite  na  ekran  poruku  o
grešci  (koristite try i except naredbe).  Također,  ako  je broj izvan
intervala [0.0 i 1.0] potrebno je ispisati odgovarajuću poruku.
'''


def gradeCalculator(grade):
    try:
        if grade >= 0 and grade < 0.6:
            print("F")
        elif grade >= 0.6 and grade < 0.7:
            print("D")
        elif grade >= 0.7 and grade < 0.8:
            print("C")
        elif grade >= 0.8 and grade < 0.9:
            print("B")
        elif grade >= 0.9 and grade <= 1.0:
            print("A")
        elif grade < 0 or grade > 1:
            raise TypeError("Not in range")
    except:
        print("Grade is not in range")


# grade = float(input("Enter grade: "))
# gradeCalculator(grade)

'''
Zadatak 4
Napišite program koji od korisnika zahtijeva unos brojeva u beskonačnoj
petlji sve dok korisnik ne upiše „Done“ (bez navodnika).
Nakon toga potrebno je ispisati koliko brojeva je korisnik unio, njihovu
srednju,  minimalnu i  maksimalnu vrijednost.  Osigurajte  program  od
krivog  unosa  (npr.  slovo  umjesto  brojke)  na  način  da  program
zanemari  taj  unos  i ispiše odgovarajuću poruku.
'''


def infiniteTilDone():
    total = 0
    max = 0
    min = 0
    count = 0
    number = 1

    while 1:
        number = input("Enter value: ")
        try:
            if number == "Done":
                print("You've entered done")
                print(total)
                print(count)
                print(min)
                print(max)
                break
            else:
                number = float(number)
                print(number)
                total += number
                count += 1
                if min == 0:
                    min = number
                elif min > number:
                    min = number
                if max < number:
                    max = number
        except:
            print("Not number")


# infiniteTilDone()


'''
Zadatak 5
Napišite  program  koji  od  korisnika  zahtijeva  unos  imena  tekstualne  datoteke. 
Program  nakon  toga  treba tražiti  linije oblika: X-DSPAM-Confidence: <neki_broj> 
koje  predstavljaju  pouzdanost  korištenog  spam  filtra.  Potrebno  je  izračunati  
srednju  vrijednost  pouzdanosti.  Koristite datoteke mbox.txt i mbox-short.txt'''


def searchConfidenceInFile():
    fname = input("Enter file name: ")

    try:
        fhand = open(fname)

        confidence = 0
        count = 0

        for line in fhand:
            line = line.rstrip()
            if line.startswith('X-DSPAM-Confidence: '):
                confidence += float(line[21:])
                count += 1

        average = confidence/float(count)
        print('Ime datoteke: ', fname)
        print('Average X-DSPAM-Confidence: ', average)
    except:
        print('File not found!')
        exit()


# searchConfidenceInFile()

'''
Zadatak 6 Napišite Python skriptu koja će učitati tekstualnu datoteku te iz redova koji 
počinju s „From“ izdvojiti mail adresu te ju spremiti u listu. Nadalje potrebno je 
napraviti dictionary koji sadrži hostname (dio emaila iza znaka @) te koliko puta se 
pojavljuje svaki hostname u učitanoj datoteci. Koristite datoteku mbox-short.txt. 
Na ekran ispišite samo nekoliko email adresa te nekoliko zapisa u dictionary-u.'''

#  From zqian@umich.edu Fri Jan  4 16:10:39 2008


def filterDomains():
    fname = input("Enter file name: ")
    domains = dict()

    try:
        fhand = open(fname)

        for line in fhand:
            line = line.rstrip()
            if line.startswith('From: '):
                line = line.split(" ")
                email = line[1]
                domain = extractDomain(email)
                if domain in domains:
                    domains[domain] += 1
                else:
                    domains[domain] = 1
        print(domains)
    except:
        print('File not found!')
        exit()


def extractDomain(email):
    domain = email.split('@')
    return domain[1]


filterDomains()
