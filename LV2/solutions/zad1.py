# ZADATAK 1.

import re


try:
    fhand = open(
        r"D:\Student\RUSU LV2\rusu-lv-2022-23\LV2\resources\mbox-short.txt")
except:
    print('File not found!')
    exit()

emails = []
text = fhand.read()
emails = re.findall("([a-zA-Z0-9.]+)@[a-zA-Z.0-9]", text)
print(emails)

# ZADATAK 2.
# 2.1.

print("//////////////    2.1     //////////////")
emails = re.findall("([a-zA-Z.0-9]+a+[a-zA-Z.0-9]+)@[a-zA-Z.]", text)
print(emails)

# 2.2
print("//////////////    2.2     //////////////")

emails = re.findall("([a-zA-Z.0-9]+a[a-zA-Z.0-9]+)@[a-zA-Z.]", text)
print(emails)

# 2.3
print("//////////////    2.3     //////////////")

emails = re.findall("([b-zB-Z.0-9]+[b-zB-Z.0-9]+)@[a-zA-Z.]", text)
print(emails)

# 2.4
print("//////////////    2.4     //////////////")

emails = re.findall("([a-zA-Z.0-9]*[0-9]+[a-zA-Z.0-9]*)@[a-zA-Z.]", text)
print(emails)

# 2.5
print("//////////////    2.5     //////////////")

emails = re.findall("([a-z.0-9]+)@[a-zA-Z.0-9]", text)
print(emails)
